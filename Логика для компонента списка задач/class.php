<?php

namespace UW;

use Bitrix\Main\UserFieldTable;
use CTasks;

class MainTaskList extends \CBitrixComponent
{
    /**
     * Метод получает ID пользовательского поля по его коду
     *
     * @param string $fieldName
     * @return mixed
     */
    private function getIdUserProp(string $fieldName)
    {
        $dbRes = UserFieldTable::getList(
            [
                'filter' => [
                    'FIELD_NAME' => $fieldName
                ],
                'select' => [
                    'ID'
                ]
            ]
        );

        return $dbRes->fetch()['ID'];
    }

    /**
     * Метод получает список статусов по ID пользовательского поля
     */
    private function getStatusList()
    {
        $nIdField = $this->getIdUserProp('UF_TASK_STATUS');

        $obUserFieldEnum = new \CUserFieldEnum();

        $dbRes = $obUserFieldEnum->GetList(
            [],
            [
                'USER_FIELD_ID' => $nIdField
            ]
        );

        $arStatusList = [];

        while ($arStatus = $dbRes->Fetch()) {
            $arStatusList[$arStatus['XML_ID']] = $arStatus;
        }

        $this->arResult['STATUS_LIST'] =  $arStatusList;
    }

    /**
     * Метод подготавливает фильтр для выборки задач, принадлежащих текущему пользователю
     *
     * @param int $userID
     * @param bool $statusID
     * @param bool $isAjax
     * @return array
     */
    private function prepareFilterForTasks(int $userID, $statusID = false, $isAjax = false) {
        $arFilter = [
            '!REAL_STATUS' => CTasks::STATE_COMPLETED,
            'UF_TASK_STATUS' => 138, // ID статуса оценки. Простите за магическое число
            '::SUBFILTER-1' => [
                '::LOGIC' => 'OR',
                '::SUBFILTER-1' => [
                    'ACCOMPLICE' => $userID, // соисполнитель
                ],
                '::SUBFILTER-2' => [
                    'RESPONSIBLE_ID' => $userID, // ответственный
                ],
                '::SUBFILTER-3' => [
                    'AUDITOR' => $userID, // наблюдатель
                ],
            ],
        ];

        if ($statusID || $isAjax)
            $arFilter['UF_TASK_STATUS'] = $statusID;

        return $arFilter;
    }

    /**
     * Метод получает список задач для текущего пользователя
     *
     * @param array $arFilter
     * @return bool
     */
    private function getTaskForCurrentUser(array $arFilter)
    {
        global $DB;

        if (\CModule::IncludeModule("tasks")) {
            $res = CTasks::GetList(
                [
                    'DEADLINE' => 'ASC'
                ],
                $arFilter
                ,
                [
                    'ID',
                    'TITLE',
                    'UF_TASK_STATUS',
                    'DEADLINE'
                ]
            );

            $arTasks = [];

            while ($arTask = $res->Fetch()) {
                $compareDateResult = $DB->CompareDates($arTask['DEADLINE'], date('d.m.Y H:i:s'));

                if($compareDateResult != abs($compareDateResult) && $arTask['DEADLINE'] ) {
                    $arTask['CLASS_OVERDUE'] = 'overdue';
                }

                $arTasks[$arTask['ID']] = $arTask;
            }

            $this->arResult['TASK_LIST'] = ($arTasks) ?: false;
        }

    }


    private function prepareResult()
    {
        global $USER;

        $userID = $USER->GetID();
        $isAjax = $this->arParams['IS_AJAX'];
        $statusID = ((int)$this->arParams['STATUS_ID'] && $isAjax) ? (int)$this->arParams['STATUS_ID'] : false;
        $arFilter = $this->prepareFilterForTasks($userID, $statusID, $isAjax);

        $this->getStatusList();
        $this->getTaskForCurrentUser($arFilter);
        $this->arResult['USER_ID'] = $userID;
    }

    public function executeComponent()
    {
        $this->prepareResult();
        $this->includeComponentTemplate();
    }
}