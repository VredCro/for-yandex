(function () {
    var Personal = {
        init: function () {
            SelectWidget.init();
            PersonalInfo.init();
            PersonalCurrentOrders.init();
            Manager.init();
        },
        showAjaxLoader: function () {
            var loader = qs('div.personal-page div.left-block div.personal-block div.tabs div.ajax-loader');
            loader.classList.add('active');
        },
        hideAjaxLoader: function () {
            var loader = qs('div.personal-page div.left-block div.personal-block div.tabs div.ajax-loader');
            loader.classList.remove('active');
        }
    };
    var SelectWidget = {
        init: function () {
            var selectTabBtns = document.qsAll('div.personal-page div.personal-block div.tabs-head div.one-tab');
            selectTabBtns.addEventListener('click', SelectWidget.selectTab);
        },
        selectTab: function () {
            this.parentNode.qs('.active').classList.remove('active');
            this.classList.add('active');
            var className = this.getAttribute('data-id');
            var block = qs('div.personal-page div.left-block div.personal-block div.tabs div.one-tab.' + className);
            SelectWidget.showTab(block);
        },
        showTab: function (tab) {
            tab.parentNode.qs('div.one-tab.active').classList.remove('active');
            tab.classList.add('active');
        }
    };
    var PersonalInfo = {
        init: function () {
            var changeBtn = qs('div.personal-page div.personal-block div.one-tab.personal button.change-btn');
            var cancelBtn = qs('div.personal-page div.personal-block div.one-tab.personal button.cancel-btn');
            var applyBtn = qs('div.personal-page div.personal-block div.one-tab.personal button.apply-btn');
            changeBtn.addEventListener('click', PersonalInfo.showChange);
            cancelBtn.addEventListener('click', PersonalInfo.hideChange);
            applyBtn.addEventListener('click', PersonalInfo.changeData);
        },
        showChange: function () {
            var changeBlock = qs('div.personal-page div.personal-block div.one-tab.personal div.change');
            var infoBlock = qs('div.personal-page div.personal-block div.one-tab.personal div.info');
            var btns = qsAll('div.personal-page div.personal-block div.one-tab.personal div.buttons button');
            Array.prototype.forEach.call(btns, function (cur) {
                cur.classList.add('active');
            });
            var changeBtn = qs('div.personal-page div.personal-block div.one-tab.personal button.change-btn');
            infoBlock.classList.remove('active');
            changeBlock.classList.add('active');
            changeBtn.classList.remove('active');
        },
        hideChange: function () {
            var changeBlock = qs('div.personal-page div.personal-block div.one-tab.personal div.change');
            var infoBlock = qs('div.personal-page div.personal-block div.one-tab.personal div.info');
            var btns = qsAll('div.personal-page div.personal-block div.one-tab.personal div.buttons  button');
            Array.prototype.forEach.call(btns, function (cur) {
                cur.classList.remove('active');
            });
            var changeBtn = qs('div.personal-page div.personal-block div.one-tab.personal button.change-btn');
            infoBlock.classList.add('active');
            changeBlock.classList.remove('active');
            changeBtn.classList.add('active');
        },
        changeData: function () {
            Personal.showAjaxLoader();
            var data = new FormData(document.forms.personalInfo);
            data.append('ajaxRequest', 'changeInfo');
            var req = new XMLHttpRequest();
            req.open('POST', window.location.href);
            req.onreadystatechange = function () {
                if (req.readyState != 4) return;
                var infoBlock = qs('div.personal-page div.personal-block div.one-tab.personal div.info');
                infoBlock.innerHTML = req.responseText;
                PersonalInfo.hideChange();
                Personal.hideAjaxLoader();
            };
            req.send(data);
        }
    };

    var PersonalCurrentOrders = {
        init: function () {
            this.initShowOrderDetail();
        },
        initShowOrderDetail: function () {
            var btns = qsAll("div.personal-page div.personal-block div.one-tab.current-orders div.order-detail");
            btns.addEventListener("click", PersonalCurrentOrders.showOrder);

            var returnBtn = qs("div.personal-page div.personal-block div.one-tab.one-order div.return-btn");
            returnBtn.addEventListener("click", PersonalCurrentOrders.returnToOrderList);

        },
        showOrder: function () {
            var tab = qs("div.personal-page div.left-block div.personal-block div.tabs div.one-tab.one-order");
            SelectWidget.showTab(tab);
        },
        returnToOrderList: function () {
            var tab = qs("div.personal-page div.left-block div.personal-block div.tabs div.one-tab.current-orders");
            SelectWidget.showTab(tab);
        }
    };

    var Manager = {
        init: function () {
            var allManagers = qsAll("div.personal-page div.right-block div.one-manager");
            for (var i = 0; i < allManagers.length; i++) {
                Manager.initManager(allManagers[i]);
            }
            var changeBtn = qs("div.personal-page div.right-block div.change-manager");
            changeBtn.addEventListener("click", Manager.showChangeManager);
        },
        initManager: function (managerBlock) {

            var showGradeBtn = managerBlock.qs("div.grade-btn");
            showGradeBtn.addEventListener("click", function () {
                Manager.gradeBtnHandler(managerBlock);
            });
        },
        gradeBtnHandler: function (managerBlock) {
            var showGradeBtn = managerBlock.qs("div.grade-btn");
            if (showGradeBtn.getAttribute("data-action") == "show") {
                Manager.showGrade(managerBlock);
                showGradeBtn.setAttribute("data-action", "send");
            } else if (showGradeBtn.getAttribute("data-action") == "send") {
                Manager.sendGrade(managerBlock);
                showGradeBtn.setAttribute("data-action", "show");
            }
        },
        showGrade: function (managerBlock) {
            var grade = managerBlock.qs("div.grade");
            var contacts = managerBlock.qs("div.contacts");
            contacts.classList.remove("active");
            grade.classList.add("active");
        },
        hideGrade: function (managerBlock) {
            var grade = managerBlock.qs("div.grade");
            var contacts = managerBlock.qs("div.contacts");
            grade.classList.remove("active");
            contacts.classList.add("active");
        },
        sendGrade: function (managerBlock) {

            Manager.hideGrade(managerBlock);
        }
    };


    document.addEventListener('DOMContentLoaded', Personal.init);
})();