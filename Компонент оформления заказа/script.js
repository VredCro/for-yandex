var Order = {
    init: function () {
        Order.initMakeOrder();
    },

    initMakeOrder: function () {
        qs('div.order div.order-btn-wrapper div.order-btn').addEventListener('click', Order.makeOrder);
    },

    setDelivery: function (id) {
        var url = Order.getUrl();
        var req = new XMLHttpRequest();
        req.open('POST', url);
        req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.onreadystatechange = function () {
            if (req.readyState != 4) return false;
            qs('div.order div.delivery-data div.delivery-info').innerHTML = req.responseText;
            Order.hideAjaxLoader();

            if (qs('.address_item .delivery_price .price_delivery_' + id))
                var deliveryPrice = parseInt(qs('.address_item .delivery_price .price_delivery_' + id).innerHTML);

            var productPrice = parseInt(qs('.price_without_delivery').value);
            if (deliveryPrice) {
                qs('.order-widget .delivery_block .price>span').innerHTML = number_format(deliveryPrice, 0, ' ', ' ');
                qs('.order-widget .delivery_block #DELIVERY_PRICE').value = number_format(deliveryPrice, 0, ' ', ' ');
                qs('.order-widget .full_price .price>span').innerHTML = number_format(deliveryPrice + productPrice, 0, ' ', ' ');
            } else {
                var fullPrice = parseInt(qs('.order-widget .full_price .price>span').innerHTML.replace(/\s/g, ''));
                var newDeliveryPrice = 0;

                if (parseInt(id) === 16) {
                    newDeliveryPrice = parseFloat($(".delivery_item.with_prepayment .delivery_price").text());
                }

                qs('.order-widget .delivery_block #DELIVERY_PRICE').value = newDeliveryPrice;
                qs('.order-widget .delivery_block .price>span').innerHTML = newDeliveryPrice;
                qs('.order-widget .full_price .price>span').innerHTML = number_format(productPrice + newDeliveryPrice, 0, ' ', ' ');
            }
        };

        req.send('ajaxAction=delivery&action=delivery&deliveryId=' + id);
    },

    getUrl: function () {
        return qs('div.order').getAttribute('data-url');
    },
    showAjaxLoader: function () {
        qs('div.ajax-loader').classList.add('active');
    },

    hideAjaxLoader: function () {
        qs('div.ajax-loader').classList.remove('active');
    },
    makeOrder: function () {
        Order.clearErrorClassFromInputs();
        Order.showAjaxLoader();
        var data = Order.getData();
        var req = new XMLHttpRequest();
        req.open('POST', Order.getUrl());
        req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        //yaCounter42931909.reachGoal('ORDER');
        req.onreadystatechange = function () {

            if (req.readyState != 4) return false;
            var resp = JSON.parse(req.responseText);
            if (resp.errors) {
                var errorString = '';
                if (resp.errors.property) {
                    resp.errors.property.forEach(function (cur) {
                        errorString += cur.message + '<br>';
                    });
                } else if (resp.errors.userExist) {
                    errorString += resp.errors.userExist[0].message + '<br>';
                    //$("input[name=go_register]")[0].checked = false;
                }

                qs('div.order div.errors').innerHTML = errorString;
                Order.hideAjaxLoader();
            } else {
                window.location.href = resp.url;
            }

        };
        req.send('action=createOrder&data=' + JSON.stringify(data));
    },

    clearErrorClassFromInputs: function () {
        var inputs = qsAll("div.order div.order-data input");
        Array.prototype.forEach.call(inputs, function (cur) {
            cur.parentNode.classList.remove("error");
        });
    },

    getData: function () {
        var data = {};
        var form = qs('div.order form');
        Array.prototype.forEach.call(form.elements, function (cur) {
            if (cur.type == 'radio' && !cur.checked) {
                return false;
            }
            var value = cur.value;

            data[cur.name] = value;
        });
        return data;
    }
};
